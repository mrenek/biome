<?php

namespace BiomeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * User
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class User implements UserInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=30, unique=true)
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=64)
     */
    private $password;

    /**
     * @ORM\OneToMany(targetEntity="Fruitua", mappedBy="egilea")
    */
    private $fruituak;

    /**
     * @ORM\OneToMany(targetEntity="Hazia", mappedBy="egilea")
    */
    private $haziak;


    public function __construct() {
        // Harremanen arrayak egin
        $this->fruituak = new ArrayCollection();
        $this->haziak = new ArrayCollection();
    }

    public function __toString() {
        return $this->username;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string 
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = password_hash($password, PASSWORD_BCRYPT);

        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }
    public function getSalt() {
    	return null;
    }
    public function getRoles() {
    	return array('ROLE_USER');
    }
    public function eraseCredentials() {
    	
    }

    /**
     * Add fruituak
     *
     * @param \BiomeBundle\Entity\Fruitua $fruituak
     * @return User
     */
    public function addFruituak(\BiomeBundle\Entity\Fruitua $fruituak)
    {
        $this->fruituak[] = $fruituak;

        return $this;
    }

    /**
     * Remove fruituak
     *
     * @param \BiomeBundle\Entity\Fruitua $fruituak
     */
    public function removeFruituak(\BiomeBundle\Entity\Fruitua $fruituak)
    {
        $this->fruituak->removeElement($fruituak);
    }

    /**
     * Get fruituak
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFruituak()
    {
        return $this->fruituak;
    }

    /**
     * Add haziak
     *
     * @param \BiomeBundle\Entity\Hazia $haziak
     * @return User
     */
    public function addHaziak(\BiomeBundle\Entity\Hazia $haziak)
    {
        $this->haziak[] = $haziak;

        return $this;
    }

    /**
     * Remove haziak
     *
     * @param \BiomeBundle\Entity\Hazia $haziak
     */
    public function removeHaziak(\BiomeBundle\Entity\Hazia $haziak)
    {
        $this->haziak->removeElement($haziak);
    }

    /**
     * Get haziak
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getHaziak()
    {
        return $this->haziak;
    }
}
