<?php

namespace BiomeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

// ASSERTING

use Symfony\Component\Validator\Constraints as Assert;

// FILE COMPONENT

use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Fruitua
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Fruitua
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="izenburua", type="string", length=150, unique=true)
     */
    private $izenburua;

    /**
     * @var string
     * 
     * @ORM\Column(name="slug", type="string", length=150, unique=true)
    */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="edukia", type="text")
     */
    private $edukia;

    /**
     * @ORM\ManyToOne(targetEntity="Ekosistema", inversedBy="fruituak") 
     * @ORM\JoinColumn(name="ekosistema_id", referencedColumnName="id")
     */
    private $ekosisteman;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="fruituak")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $egilea;

    /**
     * @ORM\OneToMany(targetEntity="Hazia", mappedBy="fruituan")
     */
    private $haziak;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ruta;

    /**
     * @Assert\File(maxSize="600000000")
     */
    private $fitxategia;

    private $temp;

    public function __construct() {
        
        $this->haziak = new ArrayCollection();

    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set izenburua
     *
     * @param string $izenburua
     * @return Fruitua
     */
    public function setIzenburua($izenburua)
    {
        $this->izenburua = $izenburua;

        return $this;
    }

    /**
     * Get izenburua
     *
     * @return string 
     */
    public function getIzenburua()
    {
        return $this->izenburua;
    }

    /**
     * Set edukia
     *
     * @param string $edukia
     * @return Fruitua
     */
    public function setEdukia($edukia)
    {
        $this->edukia = $edukia;

        return $this;
    }

    /**
     * Get edukia
     *
     * @return string 
     */
    public function getEdukia()
    {
        return $this->edukia;
    }

    /**
     * Set ekosisteman
     *
     * @param \BiomeBundle\Entity\Ekosistema $ekosisteman
     * @return Fruitua
     */
    public function setEkosisteman(\BiomeBundle\Entity\Ekosistema $ekosisteman = null)
    {
        $this->ekosisteman = $ekosisteman;

        return $this;
    }

    /**
     * Get ekosisteman
     *
     * @return \BiomeBundle\Entity\Ekosistema 
     */
    public function getEkosisteman()
    {
        return $this->ekosisteman;
    }

    /**
     * Set egilea
     *
     * @param \BiomeBundle\Entity\User $egilea
     * @return Fruitua
     */
    public function setEgilea(\BiomeBundle\Entity\User $egilea = null)
    {
        $this->egilea = $egilea;

        return $this;
    }

    /**
     * Get egilea
     *
     * @return \BiomeBundle\Entity\User 
     */
    public function getEgilea()
    {
        return $this->egilea;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Fruitua
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Add haziak
     *
     * @param \BiomeBundle\Entity\Hazia $haziak
     * @return Fruitua
     */
    public function addHaziak(\BiomeBundle\Entity\Hazia $haziak)
    {
        $this->haziak[] = $haziak;

        return $this;
    }

    /**
     * Remove haziak
     *
     * @param \BiomeBundle\Entity\Hazia $haziak
     */
    public function removeHaziak(\BiomeBundle\Entity\Hazia $haziak)
    {
        $this->haziak->removeElement($haziak);
    }

    /**
     * Get haziak
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getHaziak()
    {
        return $this->haziak;
    }

    /**
     * Fitxategiak laguntzeko funtzio sorta hemen...
     */

    public function getRutaAbsolutua()
    {
        return null === $this->ruta
            ? null
            : $this->getIgotzekoSustraiDir().'/'.$this->ruta;
    }

    public function getRutaWeb()
    {
        return null === $this->ruta
            ? null
            : $this->getIgotzekoDir().'/'.$this->ruta;
    }

    protected function getIgotzekoSustraiDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__.'/../../../../'.$this->getIgotzekoDir();
    }

    protected function getIgotzekoDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.

        $arrRuta = explode('.', $this->getRuta());

        if($arrRuta[1] == "torrent") {
            
            return 'src/BiomeBundle/Resources/public/torrents';

        } else if($arrRuta[1] == "jpg" ||
                  $arrRuta[1] == "png" ||
                  $arrRuta[1] == "jpeg") {

            return 'src/BiomeBundle/Resources/public/images';

        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload() {

        // HUTSA EGON DAITEKE

        if(null === $this->getFitxategia()) {
            return;
        }

        $this->getFitxategia()->move($this->getIgotzekoSustraiDir(), $this->ruta);

        if(isset($this->temp)) {
            unlink($this->getIgotzekoSustraiDir().'/'.$this->temp);

            $this->temp = null;
        }

        $this->fitxategia = null;
    }

    // Fitxategia bera...

    public function setFitxategia(UploadedFile $fitxategia = null)
    {
        $this->fitxategia = $fitxategia;

        if(isset($this->ruta)) {
            $this->temp = $this->ruta;
            $this->ruta = null;
        } else {
            $this->ruta = "hasierakoa";
        }

    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->getFitxategia()) {

            $filename = sha1(uniqid(mt_rand(), true));
            $this->ruta = $filename.'.'.$this->getFitxategia()->guessExtension();
        }
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        $file = $this->getRutaAbsolutua();
        if ($file) {
            unlink($file);
        }
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFitxategia()
    {
        return $this->fitxategia;
    }

    /**
     * Set ruta
     *
     * @param string $ruta
     * @return Fruitua
     */
    public function setRuta($ruta)
    {
        $this->ruta = $ruta;

        return $this;
    }

    /**
     * Get ruta
     *
     * @return string 
     */
    public function getRuta()
    {
        return $this->ruta;
    }
}
