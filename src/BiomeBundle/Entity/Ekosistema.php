<?php

namespace BiomeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * Ekosistema
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Ekosistema
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="izenburua", type="string", length=50)
     */
    private $izenburua;

    /**
     * @ORM\OneToMany(targetEntity="Fruitua", mappedBy="ekosisteman")
    */
    private $fruituak;

    public function __construct() {
        $this->fruituak = new ArrayCollection();
    }

    public function __toString() {
        return $this->izenburua;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="deskripzioa", type="string", length=150)
     */
    private $deskripzioa;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set izenburua
     *
     * @param string $izenburua
     * @return Ekosistema
     */
    public function setIzenburua($izenburua)
    {
        $this->izenburua = $izenburua;

        return $this;
    }

    /**
     * Get izenburua
     *
     * @return string 
     */
    public function getIzenburua()
    {
        return $this->izenburua;
    }

    /**
     * Set deskripzioa
     *
     * @param string $deskripzioa
     * @return Ekosistema
     */
    public function setDeskripzioa($deskripzioa)
    {
        $this->deskripzioa = $deskripzioa;

        return $this;
    }

    /**
     * Get deskripzioa
     *
     * @return string 
     */
    public function getDeskripzioa()
    {
        return $this->deskripzioa;
    }

    /**
     * Add fruituak
     *
     * @param \BiomeBundle\Entity\Fruitua $fruituak
     * @return Ekosistema
     */
    public function addFruituak(\BiomeBundle\Entity\Fruitua $fruituak)
    {
        $this->fruituak[] = $fruituak;

        return $this;
    }

    /**
     * Remove fruituak
     *
     * @param \BiomeBundle\Entity\Fruitua $fruituak
     */
    public function removeFruituak(\BiomeBundle\Entity\Fruitua $fruituak)
    {
        $this->fruituak->removeElement($fruituak);
    }

    /**
     * Get fruituak
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFruituak()
    {
        return $this->fruituak;
    }
}
