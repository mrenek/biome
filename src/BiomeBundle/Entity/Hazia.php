<?php

namespace BiomeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Hazia
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Hazia
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="iruzkina", type="string", length=255)
     */
    private $iruzkina;

    /**
    * @ORM\ManyToOne(targetEntity="User", inversedBy="haziak")
    * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
    */
    private $egilea;

    /**
     * @ORM\ManyToOne(targetEntity="Fruitua", inversedBy="haziak")
     * @ORM\JoinColumn(name="fruitua_id", referencedColumnName="id")
     */
    private $fruituan;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set iruzkina
     *
     * @param string $iruzkina
     * @return Hazia
     */
    public function setIruzkina($iruzkina)
    {
        $this->iruzkina = $iruzkina;

        return $this;
    }

    /**
     * Get iruzkina
     *
     * @return string 
     */
    public function getIruzkina()
    {
        return $this->iruzkina;
    }

    /**
     * Set egilea
     *
     * @param \BiomeBundle\Entity\User $egilea
     * @return Hazia
     */
    public function setEgilea(\BiomeBundle\Entity\User $egilea = null)
    {
        $this->egilea = $egilea;

        return $this;
    }

    /**
     * Get egilea
     *
     * @return \BiomeBundle\Entity\User 
     */
    public function getEgilea()
    {
        return $this->egilea;
    }

    /**
     * Set fruituan
     *
     * @param \BiomeBundle\Entity\Fruitua $fruituan
     * @return Hazia
     */
    public function setFruituan(\BiomeBundle\Entity\Fruitua $fruituan = null)
    {
        $this->fruituan = $fruituan;

        return $this;
    }

    /**
     * Get fruituan
     *
     * @return \BiomeBundle\Entity\Fruitua 
     */
    public function getFruituan()
    {
        return $this->fruituan;
    }
}
