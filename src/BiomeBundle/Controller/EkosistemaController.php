<?php

namespace BiomeBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use BiomeBundle\Entity\Ekosistema;
use BiomeBundle\Form\EkosistemaType;

/**
 * Ekosistema controller.
 *
 */
class EkosistemaController extends Controller
{

    /**
     * Lists all Ekosistema entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('BiomeBundle:Ekosistema')->findAll();

        return $this->render('BiomeBundle:Ekosistema:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Ekosistema entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Ekosistema();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('ekos_show', array('id' => $entity->getId())));
        }

        return $this->render('BiomeBundle:Ekosistema:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Ekosistema entity.
     *
     * @param Ekosistema $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Ekosistema $entity)
    {
        $form = $this->createForm(new EkosistemaType(), $entity, array(
            'action' => $this->generateUrl('ekos_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Ekosistema entity.
     *
     */
    public function newAction()
    {
        $entity = new Ekosistema();
        $form   = $this->createCreateForm($entity);

        return $this->render('BiomeBundle:Ekosistema:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Ekosistema entity.
     *
     */
    public function showAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $ekos = $em->getRepository('BiomeBundle:Ekosistema')->find($id);

        if(!$ekos) {
            throw $this->createNotFoundException('Ekosistema hau ez da existitzen. Beharbada Marten edo bilatu beharko zenuke.');
        }

        $fruituak = $em->getRepository('BiomeBundle:Fruitua')
                       ->findByEkosisteman($ekos->getId());

        if (!$fruituak) {
            throw $this->createNotFoundException('Ez da fruiturik aurkitu');
        }

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $fruituak,
            $request->query->get('page', 1),
            12 // LIMITEA
        );

        return $this->render('BiomeBundle:Ekosistema:show_paginated.html.twig', array(
            'entity' => $ekos,
            'pagination' => $pagination
        ));

        // $em = $this->getDoctrine()->getManager();

        // $entity = $em->getRepository('BiomeBundle:Ekosistema')->find($id);

        // if (!$entity) {
        //     throw $this->createNotFoundException('Unable to find Ekosistema entity.');
        // }

        // $deleteForm = $this->createDeleteForm($id);

        // return $this->render('BiomeBundle:Ekosistema:show.html.twig', array(
        //     'entity'      => $entity,
        //     'delete_form' => $deleteForm->createView(),
        // ));
    }

    /**
     * Displays a form to edit an existing Ekosistema entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BiomeBundle:Ekosistema')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Ekosistema entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BiomeBundle:Ekosistema:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Ekosistema entity.
    *
    * @param Ekosistema $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Ekosistema $entity)
    {
        $form = $this->createForm(new EkosistemaType(), $entity, array(
            'action' => $this->generateUrl('ekos_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Ekosistema entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BiomeBundle:Ekosistema')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Ekosistema entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('ekos_edit', array('id' => $id)));
        }

        return $this->render('BiomeBundle:Ekosistema:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Ekosistema entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('BiomeBundle:Ekosistema')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Ekosistema entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('ekos'));
    }

    /**
     * Creates a form to delete a Ekosistema entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('ekos_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
