<?php

namespace BiomeBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use BiomeBundle\Entity\Hazia;
use BiomeBundle\Form\HaziaType;

/**
 * Hazia controller.
 *
 */
class HaziaController extends Controller
{

    /**
     * Lists all Hazia entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('BiomeBundle:Hazia')->findAll();

        return $this->render('BiomeBundle:Hazia:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Hazia entity.
     * Printzipioz hau sortu botoitik erabiltzen da soilik.
     */
    public function createAction(Request $request)
    {
        $entity = new Hazia();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();

            $erab = $this->get('security.context')->getToken()->getUser();
            $entity->setEgilea($erab);

            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('hazia_show', array('id' => $entity->getId())));
        }

        return $this->render('BiomeBundle:Hazia:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }
    /**
     * Honakoa da ekosistema batean sortzeko erabiltzen den funtzioa.
     */
    public function haziaSortuAction(Request $request, $id)
    {
        $entity = new Hazia();
        $form = $this->createCreateForm($entity, $id);
        $form->handleRequest($request);

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();

            $erab = $this->get('security.context')->getToken()->getUser();
            $entity->setEgilea($erab);

            $fruitua = $em->getRepository('BiomeBundle:Fruitua')
                          ->findOneById($id);
            
            $entity->setFruituan($fruitua);

            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('fruitu_show', array('slug' => $fruitua->getSlug())));
        }

        return $this->render('BiomeBundle:Hazia:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }
    /**
     * Creates a form to create a Hazia entity.
     *
     * @param Hazia $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Hazia $entity, $id)
    {
        $form = $this->createForm(new HaziaType(), $entity, array(
            'action' => $this->generateUrl('hazia_create', array('id' => $id)),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    // *
    //  * Displays a form to create a new Hazia entity.
    //  *
     
    // public function newAction($id)
    // {
    //     $entity = new Hazia();
    //     $form   = $this->createCreateForm($entity, $id);

    //     return $this->render('BiomeBundle:Hazia:new.html.twig', array(
    //         'entity' => $entity,
    //         'form'   => $form->createView(),
    //     ));
    // }

    /**
     * Finds and displays a Hazia entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BiomeBundle:Hazia')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Hazia entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BiomeBundle:Hazia:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Hazia entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BiomeBundle:Hazia')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Hazia entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BiomeBundle:Hazia:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Hazia entity.
    *
    * @param Hazia $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Hazia $entity)
    {
        $form = $this->createForm(new HaziaType(), $entity, array(
            'action' => $this->generateUrl('hazia_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Hazia entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BiomeBundle:Hazia')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Hazia entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('hazia_edit', array('id' => $id)));
        }

        return $this->render('BiomeBundle:Hazia:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Hazia entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('BiomeBundle:Hazia')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Hazia entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('hazia'));
    }

    /**
     * Creates a form to delete a Hazia entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('hazia_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
