<?php

namespace BiomeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('BiomeBundle:Default:index.html.twig');
    }
    public function ezagutuAction() {

    	$fruituak = $this->getDoctrine()
    				->getRepository('BiomeBundle:Fruitua')
    				->findAll();

    	if (!$fruituak) {
        	throw $this->createNotFoundException(
            	'Ez da fruiturik aurkitu'
	        );
	    }

	    return $this->render('BiomeBundle:Default:ezagutu.html.twig', array(
	    	'fruituak' => $fruituak
	    ));

    }
    /*
     * Ezagutu action berria, KNP pagination bundlearekin
     */

    public function latestAction(Request $request) {

        $em         = $this->get('doctrine.orm.entity_manager');
        $kontsulta  = "SELECT fruitu FROM BiomeBundle:Fruitua fruitu";
        $query      = $em->createQuery($kontsulta);

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->get('page', 1),
            12 // LIMIT
        );

        return $this->render('BiomeBundle:Default:ezagutu_paginated.html.twig', array(
            'pagination' => $pagination
        ));

    }
}
