<?php
namespace BiomeBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
// bestela ez dabil: 
use Symfony\Component\HttpFoundation\Request;

class SecurityController extends Controller {
	public function loginAction(Request $request) {
	    $authenticationUtils = $this->get('security.authentication_utils');

	    // login errorea lortu, baldin badago
	    $error = $authenticationUtils->getLastAuthenticationError();

	    // last username 
    	// $lastUsername = $authenticationUtils->getLastUsername();

	    return $this->render(
	        'BiomeBundle:Security:login.html.twig',
	        array(
	            'error'         => $error,
	        )
	    );
	}
}