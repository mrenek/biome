<?php

namespace BiomeBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use BiomeBundle\Entity\Fruitua;
use BiomeBundle\Form\FruituaType;
use BiomeBundle\Form\FruituaEkosistematikType;

use BiomeBundle\Form\HaziaType;
use BiomeBundle\Entity\Hazia;

use Symfony\Component\HttpFoundation\Response;

// Slug :D

use Cocur\Slugify\Slugify;

/**
 * Fruitua controller.
 *
 */
class FruituaController extends Controller
{

    /**
     * Lists all Fruitua entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('BiomeBundle:Fruitua')->findAll();

        return $this->render('BiomeBundle:Fruitua:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Fruitua entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Fruitua();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
        if ($form->isValid()) {
            
            $em = $this->getDoctrine()->getManager();

            $erab = $this->get('security.context')->getToken()->getUser();
            $entity->setEgilea($erab);
            
            $slugify = new Slugify();
            $entity->setSlug($slugify->slugify($entity->getIzenburua(), '-'));

            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('fruitu_show', array('slug' => $entity->getSlug())));
        }

        return $this->render('BiomeBundle:Fruitua:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    public function ekosistemanCreateAction(Request $request, $id)
    {
        $entity = new Fruitua();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
        if ($form->isValid()) {
            
            $em = $this->getDoctrine()->getManager();

            $erab = $this->get('security.context')->getToken()->getUser();
            $entity->setEgilea($erab);
            
            $slugify = new Slugify();
            $entity->setSlug($slugify->slugify($entity->getIzenburua(), '-'));

            $entity->setEkosisteman($em->getRepository('BiomeBundle:Ekosistema')
                                       ->findOneById($id));

            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('fruitu_show', array('slug' => $entity->getSlug())));
        }

        return $this->render('BiomeBundle:Fruitua:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Fruitua entity.
     *
     * @param Fruitua $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Fruitua $entity, $id = null)
    {
        // Logika pertsonalizatua, ekosistema barrutik baldin badator,
        // automatikoki ekosisteman kanpoa ezkutatu
        if(isset($id)) {
            $form = $this->createForm(new FruituaEkosistematikType(), $entity, array(
                'action' => $this->generateUrl('fruitu_ekosisteman_sortu', array(
                    'id' => $id
                )),
                'method' => 'POST',
            ));
        } else {
            $form = $this->createForm(new FruituaType(), $entity, array(
                'action' => $this->generateUrl('fruitu_create'),
                'method' => 'POST',
            ));
        }

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Fruitua entity.
     *
     */
    public function newAction()
    {
        $entity = new Fruitua();
        $form   = $this->createCreateForm($entity);

        return $this->render('BiomeBundle:Fruitua:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Ezarritako ekosistema batean fruitu bat sortzen du
     *
     * @param id (ekosistemarena)
     *
     */
    public function ekosistemanSortuAction($id)
    {
        $entity = new Fruitua();
        $form   = $this->createCreateForm($entity, $id);

        return $this->render('BiomeBundle:Fruitua:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }
    /**
     * Finds and displays a Fruitua entity.
     *
     */
    public function showAction($slug)
    {

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BiomeBundle:Fruitua')->findOneBySlug($slug);
        $haziak = $entity->getHaziak();

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Fruitua entity.');
        }

        // $deleteForm = $this->createDeleteForm($slug);
        // CUSTOM CODE: HAZIA GEHITZEKO FORMA

        $hazia = new Hazia();
        $form = $this->haziaFormSortu($hazia, $entity->getId());

        return $this->render('BiomeBundle:Fruitua:show.html.twig', array(
            'entity'      => $entity,
            // 'delete_form' => $deleteForm->createView(),
            'haziak' => $haziak,
            'form' => $form->createView(),
        ));
    }

    /*
     * Custom hazia formAdder
     */
    private function haziaFormSortu(Hazia $entity, $id)
    {
        $form = $this->createForm(new HaziaType(), $entity, array(
            'action' => $this->generateUrl('hazia_create', array('id' => $id)),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Hazia gehitu'));

        return $form;
    }

    /**
     * Displays a form to edit an existing Fruitua entity.
     *
     */
    public function editAction($slug)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BiomeBundle:Fruitua')->findOneBySlug($slug);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Fruitua entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($slug);

        return $this->render('BiomeBundle:Fruitua:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Fruitua entity.
    *
    * @param Fruitua $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Fruitua $entity)
    {
        $form = $this->createForm(new FruituaType(), $entity, array(
            'action' => $this->generateUrl('fruitu_update', array('slug' => $entity->getSlug())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Fruitua entity.
     *
     */
    public function updateAction(Request $request, $slug)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BiomeBundle:Fruitua')->findOneBySlug($slug);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Fruitua entity.');
        }

        $deleteForm = $this->createDeleteForm($slug);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('fruitu_edit', array('slug' => $slug)));
        }

        return $this->render('BiomeBundle:Fruitua:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Fruitua entity.
     *
     */
    public function deleteAction(Request $request, $slug)
    {
        $form = $this->createDeleteForm($slug);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('BiomeBundle:Fruitua')->findOneBySlug($slug);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Fruitua entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('fruitu'));
    }

    /**
     * Creates a form to delete a Fruitua entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($slug)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('fruitu_delete', array('slug' => $slug)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
    public function torrentLortuAction($slug) {

        $ent = $this->getDoctrine()->getRepository('BiomeBundle:Fruitua')
                    ->findOneBySlug($slug);

        $edukia = file_get_contents($ent->getRutaAbsolutua());

        $response = new Response();

        $response->headers->set('Content-Type', 'text/csv');
        $response->headers->set('Content-Disposition', 'attachment;filename="'.$ent->getRuta());

        $response->setContent($edukia);

        return $response;
    }
    public function imgLortuAction($slug) {

        $ent = $this->getDoctrine()->getRepository('BiomeBundle:Fruitua')
                                   ->findOneBySlug($slug);

        $fitx = $ent->getRuta();

        return $this->render('BiomeBundle:Fruitua:showImage.html.twig', array(
            'ent'      => $ent,
            'fitx'     => $fitx
        ));
    }
}
