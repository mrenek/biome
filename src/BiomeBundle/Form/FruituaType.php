<?php

namespace BiomeBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class FruituaType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('izenburua')
            ->add('edukia')
            ->add('ekosisteman')
            // custom file field
            ->add('fitxategia')
            // deprecated, leaving as comment just in case
            // ->add('egilea')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BiomeBundle\Entity\Fruitua'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'biomebundle_fruitua';
    }
}
